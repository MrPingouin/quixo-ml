# Quixo-ML

Soon-to-be humble Quixo solver in OCaml.

LERF bitboard.

```
5  20 21 22 23 24
4  15 16 17 18 19
3  10 11 12 13 14
2   5  6  7  8  9
1   0  1  2  3  4
    a  b  c  d  e
```

## Board representation

The representation is completly inspired from chess engines FEN (Forsyth-Edwards Notation).


- We're describing board bit by bit, from top rank to bottom rank, from left file to right file.
- Player 1 is marked by 'x', and Player 2 by 'o'.
- Empty places are marked by an integer. Starting board is returning 25.
- We add one final character which is the player turn.

```
5oooo11x x

5 .....
4 oooo.
3 .....
2 .....
1 x....

  abcde
```

## UQI Protocol 

The protocol is a clone of UCI (Universal Chess Interface).

A move is represented by 2 * \[a-e\]\[1-5\] :
- The coords of the bit/dice to be cleared/removed.
- The coords of the bit/dice from where to push (if it's a file or a rank is determined by engine)

Example, moves from c1 are : c1c5, c1a1, c1e1.

Here's a basic list of instructions with their expected returned value.

```
"uqi" -> "uqiok"
"isready" -> "readyok"
"go [depth INT]" -> computed move ("c1a1")
"position startpos" -> none
"position fen" -> none
"quit" -> none

```

