let int_value c = int_of_char c - int_of_char '0';;

let explode s =
    let rec expl i res =
        if i >= String.length s then res else
            expl (i+1) (res @ [s.[i]]) in
    expl 0 [];;

let parse s = 
    let res = 0 in
    let rec parse_rec str idx last_digit res = 
        let new_line = ((idx+1) mod Bitboard.width) == 0 in
        match str with
        | [] -> res;
        | 'x' :: tl -> parse_rec tl (if new_line then (idx-9) else (idx+1)) 0 (Board.add_p1 res idx);
        | 'o' :: tl -> parse_rec tl (if new_line then (idx-9) else (idx+1)) 0 (Board.add_p2 res idx);
        | ('0' .. '9') as c :: tl -> let d = int_value c in 
                        (
                            if last_digit == 0 then (
                                let new_bit = Bitboard.offset_bit idx d
                                in 
                                (parse_rec tl new_bit d res)
                            )
                            else (
                                let previous_bit = Bitboard.offset_bit_back idx last_digit in
                                let new_bit = Bitboard.offset_bit (previous_bit) (last_digit*10+d)
                                in 
                                (parse_rec tl (new_bit) 0 res)
                                )
                        )
        | _ -> raise (Exceptions.UnexpectedTokenException "Unexpected token !");
    in
    parse_rec s 20 0 res
;;


