(* player1 is x, player2 is o *)

type board = int;;

let main () = 
    (* Tests.parsing_tests();; *)
    let () = print_string "Quixo ML \\o/\n" in
    (* let b = Parser.parse (Parser.explode "ox3x8xoox3x") in *)
    (* let moves = Board.get_moves b Color.Player1 in *)
    (* Move.print (List.nth moves 0);; *)

    let b = Parser.parse (Parser.explode "25") in
    Mcts.rollout b Color.Player1

    (* Tests.parsing_tests() *)
    (* Tests.shift_tests();; *)

(* let moves = main ();; *)

let () = Mcts.test()


