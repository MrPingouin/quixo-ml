open Color

let ucb1 state = "lol ok";;

(** [rollout] plays random moves until a player wins, or no moves available, returns a board **)
let rollout board player = 
    let rec aux b p i = 
        begin
        let () = print_string ((Board.print b) ^ "\n") in
        let score = Board.eval b in
        if score != 0 then (
            let () = Printf.printf "[%d] End game, score : %d\n" i score in
            b
        )
        else (
            let moves = Board.get_moves b p in
            let move_count = List.length moves in
            if move_count = 0 then b
            else (
                (* let x = Random.self_init() in *)
                let idx = Random.int (List.length moves) in
                let m = List.nth moves idx in
                let () = Printf.printf "[%d] Random pick move : %d/%d\n" i idx move_count in
                let () = Move.print m in
                let () = print_string "\n" in
                aux (Board.make_move b m) (if p = Player1 then Player2 else Player1) (i+1)
            )
        )
        end
    in
    aux board player 0


type 'a tree =
    | Empty
    | Node of 'a * 'a tree * 'a tree


type 'a crumb = LC of 'a tree | RC of 'a tree


type 'a zipper = Zip of 'a tree * 'a crumb list


let from_tree t = Zip(t, [])


let left z = match z with
    | Zip( Empty, _ )
    | Zip( Node(_,Empty,_), _ ) -> invalid_arg "No left child"
    | Zip( Node(_,l,_) as t, crumbs) -> Zip ( l, (LC t)::crumbs )


let right z = match z with
    | Zip( Empty, _ )
    | Zip( Node(_,_,Empty), _ ) -> invalid_arg "No right child"
    | Zip( Node(_,_,r) as t, crumbs) -> Zip ( r, (RC t)::crumbs )


let top = function
    | Zip ( _, [] ) -> invalid_arg "Already at root node"
    | Zip ( _, (LC x)::tl)
    | Zip ( _, (RC x)::tl) -> Zip ( x, tl )


let rec to_tree z = match z with
    | Zip ( n, [] ) -> n
    | Zip ( _, _) -> to_tree (top z)



let focus z value =
    let rec aux z = match z with
    | Zip ( Empty, _ ) -> None
    | Zip ( n, _) -> if n = value then Some z
                     else match (aux (left z)) with
                     | Some _ as z1 -> z1
                     | None -> match (aux (right z)) with
                         | Some _ as z2 -> z2
                         | None -> None
    in
    aux z


let str z = match z with
    | Zip (Empty, _) -> "Should never happen, zip of empty tree\n"
    | Zip ( Node(x,l,r), _ ) -> "Root : " ^ (string_of_int x) ^ "\n"


let bind = function
    | Some a -> a
    | _ -> invalid_arg "Bind called on non-Some value"


let max_ab a b = match a,b with
    | Zip(Empty, _), Zip(Empty,_) -> invalid_arg "comparing two empty zippers"
    | Zip(Empty, _), x
    | x, Zip(Empty, _) -> x
    | Zip( Node(x,_,_), _), Zip( Node(y,_,_), _) -> if x > y then a else b


let find_node z f =
    let rec aux z value = match z with
    | Zip(Node(_, Empty, Empty), _) -> f z value
    | Zip(Empty, _) -> value
    | Zip(Node(x, Empty, r), _) -> aux (f z value) (right z)
    | Zip(Node(x, l, Empty), _) -> aux (f z value) (left z)
    | Zip(Node(x, l, r), _) -> aux (aux (f z value) (right z)) (left z)
    in
    aux z z


let test () =
    let t = Node(1, Node(2,Empty,Empty), Node(3, Node(4,Empty,Empty), Empty)) in
    let needle = 4 in
    let node_to_find = Node(needle,Empty,Empty) in
    let z = from_tree t in

    (* Testing focus *)
    let () = print_string ("Testing focus, searching for " ^ (string_of_int needle) ^ "\n") in
    let Zip(Node(x,_,_),_) = bind (focus (right z) node_to_find) in
    let () = assert (x = needle) in

    (* Testing find_node *)
    let () = print_string ("Testing find_node using max_ab, should return Node of " ^ (string_of_int needle) ^ "\n") in
    let Zip(Node(x,_,_),_) = find_node z max_ab in
    let () = assert (x = needle) in

    print_string "All tests OK\n"

