all:
	ocamlopt -o quixo-ml exceptions.ml color.ml bitboard.ml move.ml board.ml mcts.ml parser.ml tests.ml main.ml

clean:
	rm *.o
	rm *.cmi
	rm *.cmx
	rm quixo-ml

