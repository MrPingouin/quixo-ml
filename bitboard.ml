(* 
   LERF bitboards

   20 21 22 23 24
   15 16 17 18 19
   10 11 12 13 14
    5  6  7  8  9
    0  1  2  3  4
*)

let width = 5
let length = 25
let top_left_idx = (width - 1) * width
let board_mask = 33554431
let border_mask = 33080895
 
(** [get_bit] returns the n-th bit of a bitboard **)
let get_bit bb n = (bb land (1 lsl n)) lsr n

(** [set_bit] sets the n-th bit of a bitboard **)
let set_bit bb n = (bb lor (1 lsl n))

(** [clear_bit] clears the n-th bit of a bitboard **)
let clear_bit bb n = bb land (lnot (1 lsl n))

(** [clear_bits] makes an XOR between bb and bits **)
let clear_bits bb bits = bb land (lnot bits)

(** [ls1b] returns the Least significant bit of a bitboard **)
let ls1b bb = bb land (-bb)

(** [print_bit] returns a 5x5 string representation of board, displaying a 1 on set bits
    Sorry for the coords mess. **)
let print board =
    let print_bit board i = if (get_bit board i) = 1 then "1" else "." in
    let rec aux acc i j =
        (* print_int (i); print_string " "; *)
        if i > length || i < 0 then "" else
            let new_line = (j mod width) = 0 in
            let rank = (i/width) in
            let previous_line_offset = (2*width)-1 in
                (print_bit board i) ^ (if new_line then ("\n" ^ (if rank=0 then "" else (string_of_int rank) ^ " ")) else "") ^ (aux acc (if new_line then (i-previous_line_offset) else (i+1)) (j+1))
    in
    "5 " ^ (aux "" top_left_idx 1) ^ "\n  abcde\n"


let modulo_bitscan_lut = [|
 -1;  0;   1;  5;  2;
 22;  6;  12;  3; 10;
 23; -1;   7; 18; 13;
 -1;  4;  21; 11;  9;
 24; 17;  -1; 20;  8;
 16; 19;  15; 14;
|]

let rec naive_bitscan bb n =
    if n == 25 then 0
    else
        if (bb land (1 lsl n)) > 0 then n
        else naive_bitscan bb (n+1)

(** [bitscan_bb] returns the current bit index using Module method **)
let bitscan bb = 
    if bb == 0 then 0
    else 
        modulo_bitscan_lut.( (ls1b bb) mod 29 )
    (* naive_bitscan bb 0 *)

(** [pop_count] returns the population count of a bitboard using Brian Kernighan's method,
 by loop counting and resetting the LS1B **)
let rec pop_count bb = 
    if bb = 0 then 0
    else 1 + (pop_count (bb land (bb-1)))

(* Functions only needed for board representation parsing *)
(* We parse board representation string from top rank to bottom rank, from left file to right file. *)
(* This means that when we shift from bit idx by a distance 'd' that exceeds the most right file, *)
(* we have to return the bit most left file from the lower rank. *)


let offset_bit idx d = 
    let start_file = (idx mod width) in
    let rank_count = ((start_file + d) / width) in
    let file_count = ((start_file + d) mod width)
    in 
    idx - rank_count * width + file_count - start_file
    


let offset_bit_back idx d = 
    let file = (idx mod width)
    in
    if (file - d) >= 0 then (idx-d) else (
        idx + (2*width - d)
    )


let rank_mask length =
    match length with 
    | 2 -> 3
    | 3 -> 7
    | 4 -> 15
    | 5 -> 31
    | _ -> raise (Exceptions.UnexpectedTokenException "Invalid rank_mask length\n")


let file_mask length =
    match length with 
    | 2 -> 33
    | 3 -> 1057 
    | 4 -> 33825
    | 5 -> 1082401
    | _ -> raise (Exceptions.UnexpectedTokenException "Invalid file_mask length\n")


(** [dec_to_bin] returns i in a binary representation **)
let dec_to_bin i =
    if i == 0 then "0" else
    let rec aux acc i =
        if i == 0 then acc else
        aux (string_of_int (i land 1) ^ acc) (i lsr 1)
    in
    (aux "" i)


