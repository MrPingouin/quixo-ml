open Move

(* We represent the board with 2 5x5 bitboards vertically stacked *)
(*
    P2(o)  45 46 47 48 49
           40 41 42 43 44
           35 36 37 38 39
           30 31 32 33 34
           25 26 27 28 29
    P1(x)  20 21 22 23 24
           15 16 17 18 19
           10 11 12 13 14
            5  6  7  8  9
            0  1  2  3  4
*)

(** [get_p1] gets all the bits of player 1 **)
let get_p1 board = board land Bitboard.board_mask

(** [get_p2] gets all the bits of player 2 **)
let get_p2 board = board lsr Bitboard.length

(** [add_p1] sets the n-th bit of board to player 1 **)
let add_p1 board n =
    assert(n >= 0 || n < Bitboard.length);
    assert(Bitboard.get_bit (get_p2 board) n = 0);
    Bitboard.set_bit board n

(** [add_p2] sets the n-th bit of board to player 2 **)
let add_p2 board n =
    assert(n >= 0 || n < Bitboard.length);
    assert(Bitboard.get_bit (get_p1 board) n = 0);
    Bitboard.set_bit board (n + Bitboard.length)

(** [get_p1] gets all the playable bits of player 1 (player2 bits + free bits) **)
let get_playable_p1 board = (lnot (get_p2 board)) land Bitboard.border_mask

(** [get_p2] gets all the playable bits of player 2 (player1 bits + free bits) **)
let get_playable_p2 board = (lnot (get_p1 board)) land Bitboard.border_mask

(** [get_player] returns the owner of the n-th bit **)
let get_player board n = 
    if (Bitboard.get_bit (get_p1 board) n) > 0 then Color.Player1
    else if (Bitboard.get_bit (get_p2 board) n) > 0 then Color.Player2
    else Color.NoPlayer


(** [get_h_moves] returns the horizontal moves from rank/file **)
let get_h_moves n player owner =
    let file = n mod Bitboard.width in
    if file > 0 && file < (Bitboard.width-1) then
        [
            Move.create ~bit:n ~mask:((Bitboard.rank_mask (Bitboard.width - file)) lsl n) ~shift:(-1) ~orig:owner ~player:player;
            Move.create ~bit:n ~mask:((Bitboard.rank_mask (file+1)) lsl (n - file)) ~shift:1 ~orig:owner ~player:player
        ]
    else if file = 0 then
        [ Move.create ~bit:n ~mask:((Bitboard.rank_mask (Bitboard.width - file)) lsl n) ~shift:(-1) ~orig:owner ~player:player]
    else 
        [ Move.create ~bit:n ~mask:((Bitboard.rank_mask (file+1)) lsl (n - file)) ~shift:1 ~orig:owner ~player:player]


(** [get_v_moves] returns the vertical moves from rank/file **)
let get_v_moves n player owner = 
    let w = Bitboard.width in
    let file = n mod w in
    let rank = n / w in
    if rank > 0 && rank < (Bitboard.width-1) then
        [
            Move.create ~bit:n ~mask:(((Bitboard.file_mask (rank+1))) lsl file) ~shift:(w) ~orig:owner ~player:player;
            Move.create ~bit:n ~mask:(((Bitboard.file_mask (w-rank)) lsl (rank * w)) lsl file) ~shift:(-w) ~orig:owner ~player:player
        ]
    else if rank = 0 then
        [ Move.create ~bit:n ~mask:((Bitboard.file_mask w) lsl file) ~shift:(-w) ~orig:owner ~player:player ]
    else
        [ Move.create ~bit:n ~mask:((Bitboard.file_mask w) lsl file) ~shift:w ~orig:owner ~player:player ]


(** [get_bit_moves] returns player's list of move from bit n **)
let get_bit_moves board player n =
    let owner = get_player board n in
    (get_v_moves n player owner) @ (get_h_moves n player owner)


(** [get_moves] returns player's list of move **)
let get_moves board player = 
    let rec aux board bb iter = 
        if bb = 0 then []
        else
            let n = Bitboard.bitscan (Bitboard.ls1b bb) in
            (get_bit_moves board player n) @ (aux board (Bitboard.clear_bit bb n) (iter+1))
    in

    if player = Color.Player1 then aux board (get_playable_p1 board) 0
    else aux board (get_playable_p2 board) 0


(** [print] returns a 5x5 string representation of board (board being a 2-stacked-bitboards
    'x' for player 1 (lower bitboard), 'o' for player 2 (upper bitboard), '.' for free cells 
    Sorry for the coords mess. **)
let print board =
    let print_player board idx =
        if (Bitboard.get_bit board idx) <> 0 then (String.make 1 Color.p1)
        else if (Bitboard.get_bit board (idx + Bitboard.length)) <> 0 then (String.make 1 Color.p2)
        else "." in

    let rec aux acc i j =
        if i > Bitboard.length || i < 0 then "" 
        else
            let new_line = (j mod Bitboard.width) = 0 in
            let rank = (i/Bitboard.width) in
            let previous_line_offset = (2*Bitboard.width)-1 in
                (print_player board i) ^ (if new_line then ("\n" ^ (if rank=0 then "" else (string_of_int rank) ^ " ")) else "") ^ (aux acc (if new_line then (i-previous_line_offset) else (i+1)) (j+1))
    in
    "5 " ^ (aux "" Bitboard.top_left_idx 1) ^ "\n  abcde\n"


(** [make_move] applies a move to board :
    1. We create the new data by masking and shifting board according to move data
    2. We clear the board using move.mask and fill the gap using data generated in 1
    3. We set the player's bit
    4. returns a board
    **)
let make_move board (m: Move.t) =
    let cleared_bits_board = Bitboard.clear_bits (get_p1 board) m.mask lor ((Bitboard.clear_bits (get_p2 board) m.mask) lsl Bitboard.length) in
    let new_bit = 1 lsl m.newbit in
    if m.shift < 0 then
        let shifted_p1 = ((m.mask land get_p1 board) lsr (-m.shift)) land m.mask in
        let shifted_p2 = (((m.mask land get_p2 board) lsr (-m.shift)) land m.mask) lsl Bitboard.length in
        cleared_bits_board lor shifted_p1 lor shifted_p2 lor new_bit
    else
        let shifted_p1 = ((m.mask land get_p1 board) lsl (m.shift)) land m.mask in
        let shifted_p2 = (((m.mask land get_p2 board) lsl (m.shift)) land m.mask) lsl Bitboard.length in
        cleared_bits_board lor shifted_p1 lor shifted_p2 lor new_bit


(** [eval_bb] returns true on win **)
let eval_bb bb =
    let full_rank_mask = Bitboard.rank_mask Bitboard.width in
    let full_file_mask = Bitboard.file_mask Bitboard.width in
    let diag1_mask = 17043521 in
    let diag2_mask = 1118480 in

    let rec aux i =
        if i = Bitboard.width then false
        else
            let r_mask = full_rank_mask lsl (i*Bitboard.width) 
            in
            ((bb land r_mask) = r_mask) || 
            let c_mask = full_file_mask lsl i 
            in
            ((bb land c_mask) = c_mask) || (aux (i+1))
    in
    (bb land diag1_mask) = diag1_mask || (bb land diag2_mask) = diag2_mask || aux 0


(** [eval] returns 1 on p1 win, -1 on p2 win **)
let eval board =
    if eval_bb (get_p1 board) then 1 else
        if eval_bb (get_p2 board) then -1 
        else 0
