open Move

let parsing_tests () = 
    let () = Printf.printf "Board parsing visual tests : \n\n" in
    let f s = 
        let () = Printf.printf "%s\n" s in
        let test = Parser.parse (Parser.explode s) in
        print_string ((Board.print test) ^ "\n")
    in
    List.iter f ["o3x"; "o4x"; "10x"; "24x"; "5oooo1x"; "5oooo11x"]
    (* o3x : all in same line *)
    (* o4x : Spaces cause a rank change *)
    (* 10x : rank change from file 0 *)
    (* 24x : placing bottom right bit (index 4) *)

let shift_tests () =
    let board = Parser.parse (Parser.explode "x22o") in
    let () = print_string ((Board.print board) ^ "\n") in
    let player = Color.Player1 in
    let n = 20 in
    let moves = Board.get_bit_moves board player n in
    let f m =
        begin
        (* let m = Move.create ~bit:n ~mask:((Bitboard.rank_mask (w - file)) lsl n) ~shift:(-5) ~orig:owner ~player:player in *)
        let () = Move.print m in
        let () = print_string ("\nmask : \n" ^ (Bitboard.print m.mask) ^ "\n") in 
        let p1 = (m.mask land Board.get_p1 board) in
        let () = print_string ("\np1 : \n" ^ (Bitboard.print p1) ^ "\n") in 
        let p2 = (m.mask land Board.get_p2 board) in
        let () = print_string ("\np2 : \n" ^ (Bitboard.print p2) ^ "\n") in 
        let new_board = Board.make_move board m in
        print_string ((Board.print new_board) ^ "\n")
        end
    in
    f (List.nth moves 1);
        

(* Bitwise operations asserts *)

assert(Bitboard.set_bit 0 2 = 4);
assert(Bitboard.get_bit (Bitboard.set_bit 0 2) 1 = 0);
assert(Bitboard.get_bit (Bitboard.set_bit 0 2) 2 = 1);
assert(Bitboard.clear_bit (Bitboard.set_bit 0 2) 2 = 0);

