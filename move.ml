open Color

type t = { bit:int; newbit:int; mask:int; shift:int; orig:Color.t; player:Color.t }

(** [bit_to_coords] returns a coords representation of one bit **)
let bit_to_coords bit =
    let ranks = [|"a";"b";"c";"d";"e"|] in
    (string_of_int (((bit mod Bitboard.length)/Bitboard.width)+1)) ^ ranks.(bit mod Bitboard.width)


let create ~bit ~mask ~shift ~orig ~player = 
    assert (bit >= 0 || bit < Bitboard.length);
    let w = Bitboard.width in
    let offset = (if player = Color.Player2 then Bitboard.length else 0) in

    let newbit = match shift with
    | -5 -> bit + (w - (bit/w) -1) * w + offset
    | -1 -> bit + (w - (bit mod w) - 1) + offset
    | 1 -> bit - (bit mod w) + offset
    | 5 -> (bit mod w) + offset
    | _ -> raise (Exceptions.InvalidMoveException "Invalid shift value") in
   
    assert (newbit >= 0);
    assert (newbit != bit);

    {
        bit;
        mask;
        shift;
        orig;
        player;
        newbit = newbit;
    }

(** [print] prints a string representation of move m. A move is a player, a mask, a source cell player and a shift direction **)
let print m = 
    let () = print_string ( begin match m.player with 
                               | Player1 -> String.make 1 Color.p1
                               | Player2 -> String.make 1 Color.p2
                               | _ -> raise (Exceptions.InvalidMoveException "Invalid move, Move.player must be Player1 or Player2")
                            end)
    in
    (* let () = Printf.printf ", bits %d %d" m.bit m.newbit in *)
    let () = Printf.printf ", %s-%s" (bit_to_coords m.bit) (bit_to_coords m.newbit) in
    let () = print_string (", [" ^ 
                            begin match m.orig with 
                               | Player1 -> String.make 1 Color.p1
                               | Player2 -> String.make 1 Color.p2
                               | _ -> "-"
                            end
                            ^ "]")
    in
    print_string (", " ^
                            begin match m.shift with
                            | 1 -> "Right"
                            | -1 -> "Left"
                            | 5 -> "Up"
                            | -5 -> "Down"
                            | _ -> raise (Exceptions.InvalidMoveException "Invalid move : invalid shift value")
                            end
     ^ "\n")
